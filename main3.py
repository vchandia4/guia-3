from sistema import Sistema
from planeta import Planeta


def main():
    solar = Sistema()
    solar.set_nombre("Solar")
    tierra = Planeta()
    tierra.set_nombre("tierra")
    tierra.set_masa(19.3)
    tierra.set_densidad(2.1)
    tierra.set_distancia(1980983.9)
    tierra.set_id(1)
    tierra.asigna_sistema(Sistema)
    print(tierra.get_nombre())
    print(tierra.get_masa())
    print(tierra.get_distancia())
    print(tierra.get_id())
    print(tierra.asigna_sistema())

    marte = Planeta()
    marte.set_nombre("marte")
    marte.set_masa(12.3)
    marte.set_densidad(1.3)
    marte.set_distancia(108373.9)
    marte.set_id(2)
    marte.asigna_sistema(Sistema)
    print(marte.get_nombre())
    print(marte.get_masa())
    print(marte.get_distancia())
    print(marte.get_id())
    print(marte.asigna_sistema())



    saturno = Planeta()
    saturno.set_nombre("saturno")
    saturno.set_masa(17.3)
    saturno.set_densidad(3.3)
    saturno.set_distancia(19357883.1)
    saturno.set_id(3)
    saturno.asigna_sistema(Sistema)
    print(saturno.get_nombre())
    print(saturno.get_masa())
    print(saturno.get_distancia())
    print(saturno.get_id())
    print(saturno.asigna_sistema)



    pluton = Planeta()
    pluton.set_nombre("plutón")
    pluton.set_masa(5.3)
    pluton.set_densidad(1.4)
    pluton.set_distancia(173983.0)
    pluton.set_id(4)
    pluton.asigna_sistema(Sistema)
    print(pluton.get_nombre())
    print(pluton.get_masa())
    print(pluton.get_distancia())
    print(pluton.get_id())
    print(pluton.asigna_sistema())



































if __name__ == "__main__":
    main
