from sistema import Sistema 


class Planeta():

    def __init__(self):
        self.__masa = None
        self.__densidad = None
        self.__distancia = None
        self.__id = None
        self.__nombre = None
        self.__sistema = None 




    def get_masa(self):
        return self.__masa



    def set_masa(self, masa):
        if isinstance(masa, float):
           self.__masa = masa
        else:
            print("El Numero ingresado no es decimal")
    

    def get_densidad(self):
        return self.__densidad



    def set_densidad(self, densidad):
        if isinstance(densidad, float):
            self.__densidad = densidad
        else:
            print("El Numero ingresado no es decimal")


    def get_distancia(self):
        return self.__distancia


    def set_distancia(self, distancia):
        if isinstance(distancia, float):
            self.__distancia = distancia
        else:
            print("El Numero ingresado no es decimal")


    def get__id(self):
        return self.__id

    def set_id(self, id):
        if isinstance(id, int):
           self.__id = id
        else:
            print("El Numero ingresado no es entero")
   
    def get_nombre(self):
        return self.__nombre

    def set_nombre(self, nombre):
        if isinstance(nombre, str):
            self.__nombre = nombre
        else:
            print("El valor ingresado no es String")
    
    def asigna_sistema(self, sistema):
        self.__sistema = Sistema()
        print(f"el sistema de {self.__nombre} es {self.__sistema.__nombre}")
        
       

    
